#!/bin/bash

python -m pytest --cov=cherryauth --cov-report term-missing --cov-config .coveragerc $@
