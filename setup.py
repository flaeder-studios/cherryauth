from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(
    name='cherryauth',
    version='0.1',
    description='Authentication plugin for CherryPy',
    long_description=readme(),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.4',
        'Topic :: Text Processing :: Linguistic',
    ],
    keywords='cherrypy oauth2 authentication',
    url='https://bitbucket.org/flaeder-studios/cherryauth',
    author='Daniel Skog',
    author_email='jda.skog@gmail.com',
    license='MIT',
    packages=['cherryauth'],
    install_requires=[
        'cherrypy',
        'pyjwt'
    ],
    setup_requires=['pytest-runner', 'pytest-cov', 'CherryPy', 'PyJWT'],
    tests_require=['pytest'],
    entry_points={
        'console_scripts': ['funniest-joke=funniest.command_line:main'],
    },
    include_package_data=True,
    zip_safe=False
)
