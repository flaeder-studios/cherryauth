import cherrypy


@cherrypy.tools.register('before_handler')
def store_referer():
    if 'referer' in cherrypy.request.headers and not cherrypy.session.get('referer'):
        cherrypy.session['referer'] = cherrypy.request.headers['referer']
