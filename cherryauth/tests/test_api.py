from unittest.mock import patch

import pytest
import jwt
import cherrypy
from cherrypy.test import helper

import cherryjwt


def parse_errors(www_auth_hdr):
    #  Bearer error="invalid_token",error_description="invalid_token"
    tokens = www_auth_hdr.split()
    tokens = tokens[1].split(',')
    tokens = [t.split('=') for t in tokens]

    return {t[0]: t[1] for t in tokens}


class App:
    @cherrypy.expose
    @cherrypy.tools.json_out()
    def index(self):
        return dict(cherrypy.request.claim)

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def claim(self):
        return repr(cherrypy.request.claim)


class TestCherrypyJWT(helper.CPWebCase):
    do_gc_test = False

    def setup_server():

        config = {
            '/': {
                'tools.jwtauth.on': True,
                'tools.jwtauth.key': 'secret'
            }
        }
        cherrypy.tree.mount(App(), '/', config=config)

    setup_server = staticmethod(setup_server)

    def test_authentication_fails_by_default(self):
        self.getPage('/', method='GET')
        self.assertStatus(401)

    def test_sends_bearer_authentication_challenge_on_failed_auth(self):
        self.getPage('/', method='GET')
        self.assertStatus(401)
        self.assertHeader('WWW-Authenticate', 'Bearer')

    def test_missing_authentication_header_value_returns_401(self):
        self.getPage('/', method='GET')
        self.assertStatus(401)
        self.assertHeader('WWW-Authenticate', 'Bearer')

    @patch('cherrypy.engine.publish')
    def test_missing_authentication_header_value_reports_on_bus(self, pub):
        self.getPage('/', method='GET')
        self.assertStatus(401)
        pub.assert_any_call(cherryjwt.AUTH_FAILED_CHANNEL, reason='Authorization header missing or not set')

    def test_authentication_header_value_not_bearer_returns_401(self):
        headers = {'Authorization': 'banana'}
        self.getPage('/', method='GET', headers=list(headers.items()))
        self.assertStatus(401)
        self.assertHeader('WWW-Authenticate', 'Bearer')

    @patch('cherrypy.engine.publish')
    def test_authentication_header_value_not_bearer_reported_on_bus(self, pub):
        headers = {'Authorization': 'banana'}
        self.getPage('/', method='GET', headers=list(headers.items()))
        self.assertStatus(401)
        pub.assert_any_call(cherryjwt.AUTH_FAILED_CHANNEL, reason='wrong scheme (banana)')

    def test_missing_token_returns_400(self):
        headers = {'Authorization': 'Bearer'}
        self.getPage('/', method='GET', headers=list(headers.items()))
        self.assertStatus(400)

    def test_valid_token_returns_200(self):
        token = jwt.encode({'some': 'payload'}, 'secret', algorithm='HS256')
        headers = {'Authorization': ' '.join(['Bearer', token.decode()])}
        self.getPage('/', method='GET', headers=list(headers.items()))
        self.assertStatus(200)

    def test_wrong_key_raises_401(self):
        token = jwt.encode({'some': 'payload'}, 'wrong_secret', algorithm='HS256')
        headers = {'Authorization': ' '.join(['Bearer', token.decode()])}
        self.getPage('/', method='GET', headers=list(headers.items()))
        self.assertStatus(401)
        error = parse_errors(dict(self.headers).get('Www-Authenticate'))
        assert 'error' in error
        assert 'error_description' in error

    @patch('cherrypy.engine.publish')
    def test_publishes_succesfull_authentications(self, pub):
        token = jwt.encode({'some': 'payload'}, 'secret', algorithm='HS256')
        headers = {'Authorization': ' '.join(['Bearer', token.decode()])}
        self.getPage('/', method='GET', headers=list(headers.items()))
        self.assertStatus(200)
        pub.assert_any_call(cherryjwt.AUTH_SUCCESSFUL_CHANNEL, claim={'some': 'payload'})

    @patch('cherrypy.engine.publish')
    def test_publishes_failed_authentications(self, pub):
        token = jwt.encode({'some': 'payload'}, 'wrong_secret', algorithm='HS256')
        headers = {'Authorization': ' '.join(['Bearer', token.decode()])}
        self.getPage('/', method='GET', headers=list(headers.items()))
        self.assertStatus(401)
        pub.assert_any_call(cherryjwt.AUTH_FAILED_CHANNEL, reason='Signature verification failed')

    def test_can_convert_claim_to_repr(self):
        token = jwt.encode({'some': 'payload'}, 'secret', algorithm='HS256')
        headers = {'Authorization': ' '.join(['Bearer', token.decode()])}
        self.getPage('/claim', method='GET', headers=list(headers.items()))
        self.assertStatus(200)
        self.assertBody('"\'some\': \'payload\'"')
