import urllib.parse
from datetime import datetime
import os.path

import cherrypy
import jwt
from cherrypy.process import plugins
import requests
import oauthlib
from requests_oauthlib import OAuth2Session
from requests_oauthlib.compliance_fixes import facebook_compliance_fix


FETCH_USER_CHANNEL = 'fetch_user'
CREATE_USER_CHANNEL = 'create_user'
NEW_TOKEN_CHANNEL = 'new_token'
ACCESS_DENIED_CHANNEL = 'access_denied'


# authenticate with oauth with the provider, add the access token to a bearer token, sign it and send it back to be used by the user.


class Provider(plugins.SimplePlugin):
    def __init__(self, bus, provider, base_uri='/api/auth/oauth2/', config={}):
        super(Provider, self).__init__(bus)

        self.provider = provider
        uri = urllib.parse.urljoin(base_uri, provider) + '/'
        self.redirect_uri = urllib.parse.urljoin(uri, 'callback')

        cherrypy.tree.mount(self, uri, config=config)

    def _create_client(self, **kwargs):
        config = cherrypy.request.app.config[self.provider]
        client_id = config.get('client_id')
        scope = config.get('scope')
        host = cherrypy.request.headers['host']
        redirect_uri = urllib.parse.urljoin('https://' + host, config.get('redirect_uri', self.redirect_uri))

        client = OAuth2Session(
            client_id=client_id,
            scope=scope,
            redirect_uri=redirect_uri,
            **kwargs
        )

        if self.provider == 'facebook':
            facebook_compliance_fix(client)

        return client

    @cherrypy.expose
    def redirect(self):
        client = self._create_client()
        authorization_base_url = cherrypy.request.app.config[self.provider]['authorization_base_url']
        auth_url, state = client.authorization_url(authorization_base_url)
        cherrypy.session['oauth2_state'] = state

        raise cherrypy.HTTPRedirect(auth_url)

    @cherrypy.expose
    def callback(self, **kwargs):
        client = self._create_client()
        cherrypy.request.social = client
        token_config = cherrypy.request.app.config['token']
        provider_config = cherrypy.request.app.config[self.provider]

        token_alg = token_config.get('token_algorithm', 'HS256')
        token_secret = token_config.get('token_secret')
        token_expiration_time = token_config.get('token_expiration_time')
        token_audience = token_config.get('token_audience')
        token_issuer = token_config.get('token_issuer')
        token_domain = token_config.get('token_domain')
        token_url = provider_config['token_url']
        client_id = provider_config['client_id']
        client_secret = provider_config['client_secret']
        cookie_key = provider_config.get('cookie_key', 'jwt-token')

        try:
            oauth2_token = client.fetch_token(token_url, client_id=client_id, client_secret=client_secret, **kwargs)
        except (requests.HTTPError, oauthlib.oauth2.OAuth2Error, ValueError) as e:
            error = str(e)
            cherrypy.engine.publish(ACCESS_DENIED_CHANNEL, provider=self.provider, error=error, **kwargs)
            raise cherrypy.HTTPError(401, error)

        claim = dict(
            provider=self.provider,
            token=oauth2_token,
            iat=datetime.utcnow()
        )

        user_data = cherrypy.engine.publish(FETCH_USER_CHANNEL, self.provider, token=oauth2_token)
        if not user_data:
            user_data = cherrypy.engine.publish(CREATE_USER_CHANNEL, self.provider, token=oauth2_token)

        claim['user'] = user_data.pop() if user_data else None

        if token_expiration_time:
            claim['exp'] = datetime.utcnow() + token_expiration_time
        if token_audience:
            claim['aud'] = token_audience
        if token_issuer:
            claim['iss'] = token_issuer

        if os.path.isfile(token_secret):
            with open(token_secret) as f:
                token_secret = f.read()

        jwt_token = jwt.encode(claim, key=token_secret, algorithm=token_alg).decode()
        cherrypy.engine.publish(NEW_TOKEN_CHANNEL, token=jwt_token, claim=claim)

        cherrypy.response.cookie[cookie_key] = jwt_token
        cherrypy.response.cookie[cookie_key]['secure'] = cherrypy.request.app.config[self.provider].get('cookie_secure', 1)
        cherrypy.response.cookie[cookie_key]['httponly'] = cherrypy.request.app.config[self.provider].get('cookie_httponly', 1)
        cherrypy.response.cookie[cookie_key]['max-age'] = cherrypy.request.app.config[self.provider].get('cookie_max-age', 3600)
        cherrypy.response.cookie[cookie_key]['path'] = cherrypy.request.app.config[self.provider].get('cookie_path', '/')
        if token_domain:
            cherrypy.response.cookie[cookie_key]['domain'] = token_domain
        referer = cherrypy.session.get('referer', '/')
        cherrypy.lib.sessions.expire()

        raise cherrypy.HTTPRedirect(referer)
