import os.path

import cherrypy
from cherrypy.process import plugins
from cherryjwt import SECRET_CHANNEL


@cherrypy.expose
class TokenKeyHandler(plugins.SimplePlugin):

    def __init__(self, bus, config=None, base_uri='/api/auth/token/'):
        super(TokenKeyHandler, self).__init__(bus)

        self.secret = None

        cherrypy.tree.mount(self, base_uri, config=config)

    def start(self):
        secret = self.get_cert()
        cherrypy.engine.publish(SECRET_CHANNEL, secret)

    def get_cert(self, kid=None):
        if not self.secret:
            config = cherrypy.request.app.config['token']

            # use token_public_key if an assymmetric crypto is used, otherwise use the token_secret
            key = config.get('token_public_key')

            # if a file is provided, read it
            if os.path.isfile(key):
                with open(key) as f:
                    key = f.read()

            # if no key is provided, this handler should not exist
            if not key:
                raise cherrypy.HTTPError(500, 'token_public_key not defined...')

            self.secret = key

        return self.secret

    @cherrypy.expose
    def cert(self):
        return self.get_cert()
