import urllib.parse
import os.path

import cherrypy
from cherrypy.process import plugins
import cherryjwt


class TokenChecker(plugins.SimplePlugin):
    def __init__(self, bus, base_uri='/api/auth/', config={}, secret=None):
        super(TokenChecker, self).__init__(bus)

        uri = urllib.parse.urljoin(base_uri, 'check') + '/'
        cherrypy.tree.mount(self, uri, config=config)

    @cherrypy.tools.jwtauth()
    @cherrypy.expose
    def default(self, *args, **kwargs):
        pass
