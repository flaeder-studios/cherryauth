import logging

import cherrypy
from cherryjwt import AUTH_SUCCESSFUL_CHANNEL, AUTH_FAILED_CHANNEL, SECRET_CHANNEL

from cherryauth import Provider, TokenKeyHandler, FETCH_USER_CHANNEL, TokenChecker


# logging.basicConfig(level=logging.ERROR)
# logging.getLogger('requests_oauthlib').setLevel(logging.DEBUG)


cherrypy.tools.jwtauth.audience = 'all'


with open('jwtRS256.key.pub') as f:
    public_key = f.read()


class App:

    @cherrypy.expose
    def login(self):
        return """
            <!DOCTYPE html>
            <html>
            <head>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            </head>
            <body>
                <a href="/api/auth/oauth2/facebook/redirect/">Facebook</a>
            </body>
            </html>
        """

    @cherrypy.expose
    @cherrypy.tools.store_referer()
    @cherrypy.tools.jwtauth(redirect_to='/login', key=public_key, audience='all')
    def index(self):
        return """
            <!DOCTYPE html>
            <html>
            <head>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            </head>
            <body>
                Hello {name}!<br>
                <a href="/api/auth/oauth2/check/">Check</a>
            </body>
            </html>
        """.format(**cherrypy.request.claim.user)


Provider(cherrypy.engine, provider='facebook', config='app.conf').subscribe()
TokenKeyHandler(cherrypy.engine, config='app.conf')
TokenChecker(bus=cherrypy.engine, config='app.conf')


def print_fail(reason=None):
    print('authentication failed ({})'.format(reason))


def print_success(claim=None):
    print('authentication ok ({})'.format(claim))


def fetch_user(provider=None, token=None):
    facebook = cherrypy.request.social
    r = facebook.get('https://graph.facebook.com/me?fields=email,name')
    return r.json()


cherrypy.engine.subscribe(AUTH_FAILED_CHANNEL, print_fail)
cherrypy.engine.subscribe(AUTH_SUCCESSFUL_CHANNEL, print_success)
cherrypy.engine.subscribe(FETCH_USER_CHANNEL, fetch_user)
cherrypy.engine.publish(SECRET_CHANNEL, public_key)


def main():
    cherrypy.quickstart(App(), '/', 'app.conf')


__name__ == '__main__' and main()
