# CherryAuth
CherryPy Authentication Swiss Army Knife


# Overview #
The CherryAuth tool is a CherryPy API that can be used to authenticate users via OAuth2, for example via facebook.
The tool stores the OAuth2 access token in a JSON Web Token in a cookie. The excellent pyjwt package is used to this end.
Uses requests-oauthlib for authentication and interaction with the Oauth2 provider (e.g. facebook.com).


## Features ##
- Basic authnetication (TBD)
- Digest authentication (TBD)
- Negotiate authentication (e.g. Kerberos) (TBD)
- OAuth2 authentication
- OAuth2 token renewal (TBD)
- JWT generation
- Public certificate hosting
- Referer redirection
- User management backend extension points
- JWT revocation management (TBD)
- Certificate management (TBD)

## Installation ##


# Example #
The plugin can be readily deployed, as demonstrated below. The cherryjwt handles JWT authentication with cherrypy.

## Prerequisites ##
- Facebook app registered (client secret and id)
- RSA key, see below
- TLS certificate, see below

## Generation of certificates ##
RSA keys can be generated using the commands below.

    ssh-keygen -t rsa -b 2048 -f jwtRS256.key
    openssl rsa -pubout -in jwtRS256.key -out jwtRS256.key.pub

x509 keys for https can be generated as below.

    openssl req -sha256 -newkey rsa:4096 -nodes -keyout key.pem -x509 -out cert.pem

## Server ##
See __main__.py

## Config ##
See app.conf

# Configuration #


# Integration #


# API #
